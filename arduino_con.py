#!/usr/bin/env python3

import serial as sr
import time

ser_nano = None


def write_nano( _msg):
   
    global  ser_nano 

    if ser_nano is None :
     ser_nano = sr.Serial('/dev/ttyUSB0', 9600, timeout=1)
    to_arduino = str((_msg)).encode('utf-8')
    #ser_nano.reset_input_buffer()
   # ser.write(str(_msg).encode('utf-8'))
 
    #while True:
    
    ser_nano.write(to_arduino)
   
   
    


def read_nano( ): 

    global  ser_nano 

    if ser_nano is None :
     ser_nano = sr.Serial('/dev/ttyUSB0', 9600, timeout=1)   
    while True:
     if ser_nano.in_waiting > 0:
          
      line = ser_nano.readline().decode('utf-8').rstrip()
   
    
      return line 
      
   


""" 
def main():
 
  write_nano(19)
  print(read_nano())
  
if __name__ == '__main__':
     main()       """