from anyio import Path
from models import *
import time
import gi
from gi.repository import GLib, GObject, Gst, GstBase, GstVideo, Gtk


import threading
gi.require_version('GstVideo', '1.0')
gi.require_version('Gst', '1.0')

from snif import *
from app_settings import *
import socket
import tempfile
import os
import subprocess

import glob
from PIL import Image

Encode_ID = None

###########################################################________________________________#####################################################
def snif_stream(_self):
      print('Starting a  SNIFFER..')
      loop_on = True
      while loop_on:
        time.sleep(4)
        print("\n SNIF URL"+_self.rtsp.strip())
        ret= get_rtsp_status(_self.rtsp.strip())
        print("\n sniffer response is "+str(ret))
        if(ret==1):
            _self.connect_Ip_cam()
            print("Sniffer Exit")
            loop_on = False
            print("\n ###################  Stream FOUND EXITING SNIFF ##################### \n")

#######################################################



def socketServer(_self):
    HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
    PORT = 11200  # Port to listen on (non-privileged ports are > 1023)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
        conn, addr = s.accept()
        with conn:
            print(f"Connected by {addr}")
            while True:
                data = conn.recv(1024)
                print ("on_server \n")
                msg = data.decode()
                process_msg(msg, _self)
                print(data)
                print ("on_server \n")
                if not data:
                    break
                conn.sendall("Done".encode("utf-8") )


def process_msg(_msg, _self):

    word=_msg.split(',')

    if word[0] == "Snap":
        print ("Snap   asked " )
        _self.Snap_and_thumb(word[1], word[2],0)

    if word[0] == "Stop":
        print ("Stop  record asked " )
        _self.StopRecord( )
    
# Snap_and_thumb(self, _job_id,  _name_path, _isVideo):

    if word[0] == "Record":
      
        _self.StartRecord(word[1], word[2], word[3])

   

###################################################################################################################################


############################################################################################



class Cam():

    def __init__(self,  _xid, ):  

        self.proj_id = Project_ID
 
        self.vbox_container =  None
        row = get_project_by_id_internal(Project_ID)

        self.name = row[1]
        self.rtsp = row[2]
     
       # self.cam_pass = row[9]
       # self.cam_ip = row[10]
       # self.FilePath = row[4].strip()
       #self.snapPath = row[5].strip()
        
        self.State = None
        self.threadLoop= None
        self.recordLock=False
        self.snap_name="default"
        self.threadLoop = None
      
      
        
        self.live_xid = _xid
        #self.pipe = _pipe
        
        self.BasePipeLine = None
        self.RecPipeLine = None
        self.state = Gst.State.NULL
        self.FallBackLine = None
        self.ip_cam_on_staus = False 

        
        self.image_url = 'filesrc location=fb.png ! video/x-raw,width=1440,height=720 !  decodebin ! videoconvert ! imagefreeze ! autovideosink'

        self.Main_url = ' rtspsrc location=' + self.rtsp + ' latency=0 buffer-mode=2 !  \
            rtpjitterbuffer latency=100 !  rtph264depay ! h264parse ! avdec_h264 ! videoconvert ! tee name=tr  ! \
                 tee name=tp ! tee name=tpng ! queue name=dispqueue leaky=1 ! autovideosink sync=false'

          
#  gst-launch-1.0 -v videotestsrc ! video/x-raw, pixel-aspect-ratio=(fraction)4/3 ! videoscale ! ximagesink

        self.fallback()
        

    
    def  connect_Ip_cam(self ): 

        if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)

        self.ip_cam_on_staus = True 
        self.connect(self.Main_url )

    def  connect_image(self ): 
        
        if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)

        self.ip_cam_on_staus = False 
        self.connect(self.image_url )  
 


    def   connect(self, _pipeline):


            self.BasePipeLine = Gst.parse_launch(_pipeline )
            bus = self.BasePipeLine.get_bus()
            bus.add_signal_watch()
            bus.enable_sync_message_emission()
            bus.connect('sync-message::element', self.on_cam_sync_message)
            bus.connect("message", self.on_message)
            ret = self.BasePipeLine.set_state(Gst.State.PLAYING)
            if ret == Gst.StateChangeReturn.FAILURE:
                print("\n ##################  ERROR : Unable to set the pipeline to the playing state")
                #sys.exit(1)
                self.fallback()
           # else:
                #   if(self.loopTh is not  None ):
                  #  self.isFallBack = False
            self.State = Gst.State.PLAYING

    def  fallback(self):

            self.isFallBack = True
            if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)
            if(self.ip_cam_on_staus):
              self.ip_cam_on_staus = False
              if(self.BasePipeLine is not None):
                self.BasePipeLine.set_state(Gst.State.NULL)
              print("ON FALL BACK >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ##################################### /n")
            self.ip_cam_on_staus = False 
            self.connect(self.image_url )  
           
            print("\n This is Fallback Stream ON NOW ")
            self.loopTh= threading.Thread(target=snif_stream, args=(self,), daemon=True)
            self.loopTh.start()
        # #################
    
    def on_message(self, bus, message):
        t = message.type
        if t == Gst.MessageType.ERROR:
            err, dbg = message.parse_error()
            print("ERROR:", message.src.get_name(), ":", err)
            if dbg:
                print("Debug info:", dbg)
            self.terminate = True
        elif t == Gst.MessageType.EOS:
            print("End-Of-Stream reached")
           
            self.terminate = True
        elif t == Gst.MessageType.STREAM_START:
             print("\n ###############      MESSAGE_STREAM_START           ####### \n ")
             self.State = Gst.State.PLAYING
             if(self.threadLoop is None):
                self.threadLoop = threading.Thread(target=socketServer, args=( self,))
                self.threadLoop.start()
             
      
             
          
        elif t == Gst.MessageType.DURATION_CHANGED:
            # the duration has changed, invalidate the current one
            self.duration = Gst.CLOCK_TIME_NONE
        elif t == Gst.MessageType.STATE_CHANGED:
            old_state, new_state, pending_state = message.parse_state_changed()
          
            if(Gst.Element.state_get_name(new_state)=="PLAYING"):
                 self.playing = new_state == Gst.State.PLAYING    



   
 ##################################################################################################################################


    def StopRecord(self, ):
        
        

        if(self.RecPipeLine ):
         self.RecPipeLine.send_event(Gst.Event.new_eos()) 
         time.sleep(3)              
         self.RecPipeLine.set_state(Gst.State.NULL)  
         self.recordLock = False  
         self.RecPipeLine  = None
         if(Encode_ID):
            self.Encode(Encode_ID)
         
         
      #pip install ffmpeg-python
      #ffmpeg -i 1212_10142022_173612.mp4 -c copy -movflags +faststart output.mp4
    def Encode(self,  db_id): 
        #get_file_media_path(  _fid):
        db_path = get_file_media_path(db_id)[0]
        fname = os.path.basename(db_path)
        f_path = os.path.dirname(db_path)

        print("File      Path:", f_path)
        
        fpout =f_path+"/enc_"+fname
        

        command = "ffmpeg -i "+db_path+" -c copy -movflags +faststart "+fpout
        subprocess.call(command,shell=True)
       
       # ffprobe -i enc_1212_10172022_1234.mp4 -v quiet -show_entries format=duration -hide_banner -of default=noprint_wrappers=1:nokey=1  
   
        command = "ffprobe -i "+fpout+" -v quiet -show_entries format=duration -hide_banner -of default=noprint_wrappers=1:nokey=1 "

        p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()## Wait for date to terminate. Get return returncode ##
        p_status = p.wait()

        print("Command output : ",output.decode())
        print("Command exit status/return code : ", p_status)
        
        # update DB with new path

        update_clip( db_id, fpout, output.decode())
        ## remove un  encoded file 
        os.remove(db_path)



    def StartRecord(self,  _job_id,  _name_path, _fDescp):
        
         
          if self.recordLock == False:
            self.recordLock=True 
          
            strPipe="rtspsrc location="+self.rtsp+" !  application/x-rtp, clock-rate=90000,payload=96 !  \
                 rtph264depay ! h264parse  ! avdec_h264 ! videoconvert ! x264enc pass=quant ! h264parse ! \
                   video/x-h264, profile=main  ! qtmux ! \
                filesink  location="+_name_path.strip() 
            self.RecPipeLine = Gst.parse_launch(strPipe)
            ret = self.RecPipeLine.set_state(Gst.State.PLAYING)
            if ret == Gst.StateChangeReturn.FAILURE:
                print("\n ##################  ERROR : Unable to set the pipeline to the playing state")
                #sys.exit(1)
            else:  
               # thumbs_path = os.getcwd()+"/media/"+os.path.basename(_self.snapPath)
             
             self.Snap_and_thumb( _job_id, _name_path, 1 )
             print("\n Recording of video started ")  
             
  
     #################################################################################---  Snap thumb part               
  
    def Snap_and_thumb(self, _job_id,  _name_path, _isVideo):


        self.snapTH = threading.Thread(target=TH_snap, args=(self, _job_id, _name_path.strip(), _isVideo ))
        self.snapTH.start()
        self.snapTH.join()
       # _button.set_sensitive(True)
        self.snapTH = None

    def on_cam_sync_message(self, bus, msg):
        if msg.get_structure().get_name() == 'prepare-window-handle':
           # print('Live Window handle Set Done')
            msg.src.set_window_handle(self.live_xid)
     ################################------------Must for snap and recording       
    def probe_block(self, pad, buf):
            print('recording probe_block >')
            return True        
                      
    ##################################################Thread to Snap Image #################################



##################################################Thread to Snap Image #################################
""" def  TH_snap2( _self, _job_id, _snap_path, _is_video):
              
              snap_path = _snap_path
              if(_is_video == 1):
                temp_name = next(tempfile._get_candidate_names()) 
                snap_path = os.getcwd()+"/temp/"+temp_name+".png"
               
              snap_pipe="rtspsrc location="+_self.rtsp+" ! rtph264depay  ! avdec_h264 ! jpegenc snapshot=TRUE ! filesink location= "+snap_path  
              SnapPipeLine = Gst.parse_launch(snap_pipe)
              ret = SnapPipeLine.set_state(Gst.State.PLAYING) 
              ret =SnapPipeLine.set_state(Gst.State.PLAYING)
              if ret == Gst.StateChangeReturn.FAILURE:
                print("\n ##################  ERROR : Unable to Generate Thumbnail")
                #sys.exit(1)
              else:
                print("\n Done")
                time.sleep(2)
                
                thumbs_name =  os.path.basename(snap_path)
                thumbs_path = os.getcwd()+"/media/"+os.path.basename(snap_path)
                thumbnails(snap_path, thumbs_path) 
                insert_clip( _snap_path, _job_id, "media/"+thumbs_name, _is_video)  
                if SnapPipeLine :
                  SnapPipeLine.set_state(Gst.State.NULL) 
 """


def thumbnails(_src_path,  _des_path):
   try:
      image = Image.open(_src_path)
      image.thumbnail((200,200))
      image.save(_des_path)
      return 1
      #image1 = Image.open('images/thumbnail.jpg')
      i#mage1.show()
   except IOError:
      pass 

 

def  TH_snap( _self, _job_id, _snap_path, _is_video):

        global Encode_ID
        Encode_ID = None 
        pipeSnap = None
        imgqueue = None
        snapName= _snap_path
        date = datetime.now()
        clip_time =  date.strftime("%Y-%m-%d %H:%M")# datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

        if(_is_video == 1):
                temp_name = next(tempfile._get_candidate_names()) 
                snapName = os.getcwd()+"/temp/"+temp_name+".png"

        try:

         if(_self.State == Gst.State.PLAYING):
             pipeSnap = Gst.parse_bin_from_description("queue name=imgqueue leaky=2 ! jpegenc ! filesink location="+snapName ,True)

             # Add Bin To Line
             if( _self.BasePipeLine is not None):
               _self.BasePipeLine.add(pipeSnap)
               _self.BasePipeLine.get_by_name("tpng").link(pipeSnap)
               ret = pipeSnap.set_state(Gst.State.PLAYING)
               time.sleep(2)
               #clean after Image capture
               imgqueue =  pipeSnap.get_by_name("imgqueue")
               imgqueue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, _self.probe_block)
               _self.BasePipeLine.get_by_name("tpng").unlink(pipeSnap)
               imgqueue.get_static_pad("sink").send_event(Gst.Event.new_eos())
               if ret == Gst.StateChangeReturn.FAILURE:
                            print("ERROR: Unable to Start Recording")
                            _self.TerminateRecording=True
               else:
                #make snap of this image 
                thumbs_name =  os.path.basename(snapName)
                thumbs_path = os.getcwd()+"/media/"+os.path.basename(snapName)
                thumbnails(snapName, thumbs_path) 
               db_id =  insert_clip( _snap_path, _job_id, "media/"+thumbs_name, _is_video)  
               if(_is_video == 1):
                 Encode_ID =db_id

              # pipeSnap.set_state(Gst.State.PAUSED)

               #pipeSnap  = None

        finally:


            if pipeSnap is not None:

                # Block new data
                filequeue = pipeSnap.get_by_name("imgqueue")
                filequeue.get_static_pad("src").add_probe(Gst.PadProbeType.BLOCK_DOWNSTREAM, _self.probe_block)

                # Disconnect the recording pipe
                _self.BasePipeLine.get_by_name("tpng").unlink(pipeSnap)

                # Send a termination event to trigger the save
                filequeue.get_static_pad("sink").send_event(Gst.Event.new_eos())

                # Clear the reference to the pipe
               # pipeSnap.set_state(Gst.State.NULL)
                pipeSnap = None
                print("\n finally: Captured exited \n")


  
############################################################################################
      
