# Python program to explain os.path.islink() method
	
# importing os.path module
import os.path

# Path
path = "/run"

# Check whether the
# given path is a
# mount point or not
ismount = os.path.ismount(path)



# Path
path = "/media/muji/usb"

# Check whether the
# given path is a
# mount point or not
ismount = os.path.ismount(path)
print(ismount)
