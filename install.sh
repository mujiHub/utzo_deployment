#!/bin/bash
sudo apt-get install libx264-dev libjpeg-dev sudo apt-get install \
libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev \
gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly \
gstreamer1.0-libav  gstreamer1.0-tools gstreamer1.0-x gstreamer1.0-alsa gstreamer1.0-gl gstreamer1.0-gtk3
sudo apt install sqlitebrowser
sudo apt install python3-pip
pip3 install fastapi
pip3 install Jinja2
pip3 install uvicorn
pip3 install sqlalchemy
pip3 install pywebview
pip3 install --upgrade onvif_zeep