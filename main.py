import gi
gi.require_version('Gtk', '3.0')
gi.require_version("WebKit2", "4.0")	# Web content engine
from gi.repository import Gtk, WebKit2, Gdk, Gst
gi.require_version('Gst', '1.0')


import threading
import time
import multiprocessing

import os
import cairo

from gi.repository import Notify
from webprocess import run_api


from cam import Cam
from app_settings import *







#########################################################
PiCam = None
b_EXIT = False 
pipe_from_web = None

def setEXIT(value):
     global b_EXIT   #declare a to be a global
     b_EXIT = value  #this sets the global value of a

class Handler:
   
    
    
    
    def onDestroy(self, *args):
        PiCam.close()
        Gtk.main_quit()

    
    def  onRealize_player(self, _widget):
          print("on Realized is called ")
          live_xid=_widget.get_property('window').get_xid()
         
         # PiCam=Camera(CAM_ID, live_xid , pipe_from_web)
         
          PiCam = Cam( live_xid )
         
    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)   
""" 
   
    def on_key_press_event(self, widget, event):

   
        if(Gdk.keyval_name(event.keyval)=="F2"):
         print("F2 pressed")
         # creating a pipe
        builder = Gtk.Builder()
        builder.add_from_file("gui/webview.glade")
    #builder.connect_signals(Handler())

        window = builder.get_object("webmain")
        window.set_default_size(600,800)
       
        window.set_position ( Gtk.WindowPosition.CENTER_ALWAYS)

   
        wBox = builder.get_object("webbox")

        web_view = WebKit2.WebView()				# initialize webview
        web_view.load_uri('http://0.0.0.0:10000/')	# default homepage
        scrolled_window = Gtk.ScrolledWindow()		# scrolling window widget
    
        scrolled_window.add(web_view)
        wBox.pack_start(scrolled_window, True, True, 0)5
        window.show_all()
        Gtk.main() """




def run_function():
    
    Gst.init(None)
  
    print("Cam Runner ")
    builder = Gtk.Builder()
    builder.add_from_file("templates/main_wind.glade")
    builder.connect_signals(Handler())
   
    window = builder.get_object("main_win")
    window.set_title("UTZO")
    screen = window.get_screen()
    visual = screen.get_rgba_visual()
    if visual and screen.is_composited():
            window.set_visual(visual)
    # Create view for webpage
    #scrollLeft
    #scrollTop
    left_panel = builder.get_object("scrollLeft")
    top_panel = builder.get_object("scrollTop")

    web_view_left = WebKit2.WebView()

    web_view_top = WebKit2.WebView()



   

    #web_view_top.load_uri('http://0.0.0.0:10000/sensor')
    web_view_top.load_uri('http://0.0.0.0:10000/top')
    top_panel.add(web_view_top)

    web_view_left.load_uri('http://0.0.0.0:10000/left')
    left_panel.add(web_view_left)


    
    lst=getScreensize()
   
    window.set_default_size(lst[0], lst[1])
   # window.set_decorated (False)
    window.set_keep_above(True) 
    #window.fullscreen()
    window.show_all()
    Gtk.main()
    
       

def thread_status(name):

    while not b_EXIT:
        
         time.sleep(5)
##################################################Thread to listen on pipe from web API #################################




if __name__ == "__main__":
    #create_folders()
    #parent_conn, child_conn = multiprocessing.Pipe() 
    x = threading.Thread(target=thread_status, args=(5,))

    p2 = multiprocessing.Process(target=run_api, args=( ))

   
    p2.start()
  
    p2.join(0.25) 
    print("Cam Closed ") 
    
    x.start()
    run_function()
  