# Version 3.4
import sqlite3
import os
import datetime
# Connecting to sqlite
# connection object

database = r"hook.db"
 
def get_project_by_id(   _proj_id):
     
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM projects WHERE id=?", (_proj_id,))

    query_result = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result
   
#############################################################################
def get_project_by_id_internal(   _proj_id):
     
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM projects WHERE id=?", (_proj_id,))

    query_result = cur.fetchall()
  
    return query_result[0]


def get_file_media_path(  _fid):

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT path FROM clips WHERE id=?", (_fid,))

    query_result = cur.fetchall()
  
    return query_result[0]


def get_project_media_path(  _id):

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT media_path FROM projects  WHERE id=?", (_id,))

    query_result = cur.fetchall()
    print(query_result)
  
    return query_result[0][0]    


    

###############################################################################

def get_default_project():
     
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT load_id FROM load_default LIMIT 1")

    query_result = cur.fetchall()
   # query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    res =query_result[0][0]
    return res
###############################################################################

def update_load( _id):

    data_set=(_id,1)

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
       
        cur.execute("UPDATE  load   SET  load_id = ? WHERE id = ? ",data_set)
    conn.commit()
    conn.close()
    
    return 1
############################################
def is_exist_job( _id): 

    data_set=(_id)
    directory = os.path.dirname(os.path.abspath(__file__))
     # SELECT EXISTS(SELECT 1 FROM myTbl WHERE u_tag="tag");
    res=0
    conn = sqlite3.connect(directory+"/"+database)
    with conn: 
        cur =  conn.cursor()
        cur.execute("SELECT id FROM projects WHERE id = ? ", (_id,))
        exist = cur.fetchall()
        if exist is None:  
            res = 0
        else:
            res = 1
    conn.commit()
    conn.close()
    
    return res    


######################################################################################------------- Insert project
def insert_job( _id, _name, media_path):

    data_set=(_id, _name,  media_path+"/")

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT id FROM projects WHERE id = ? OR name = ?", (_id, _name))
        exist = cur.fetchone()
        if exist is None:
         cur.execute("INSERT INTO projects( id ,name,  media_path) values(?,?, ?)",data_set)
         res = "success"
        else:
         res = "already exist"    

    conn.commit()
    conn.close()
    
    return res

""" 
 cursor.execute("SELECT rowid FROM components WHERE name = ?", (name,))
    data=cursor.fetchall()


    exist = cursor.fetchone()
if exist is None:
  ... # does not exist
else:
  ... # exists

 """
######################################################################################------------- Update project
def update_project( _id,_name, _details, _media_path):

    data_set=(_name, _details,   _media_path, _id)

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
       
        cur.execute("UPDATE  projects   SET   name = ?, details = ?, media_path = ? WHERE id = ? ",data_set)
    conn.commit()
    conn.close()
    
    return 1


######################################################################################------------- Update Clips
def update_clip( _id, _media_path, _videoLength):

    data_set=(  _media_path, _videoLength, _id)

    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
       
        cur.execute("UPDATE  clips   SET    path = ?, videoLength =? WHERE id = ? ",data_set)
    conn.commit()
    conn.close()
    
    return 1
""" 
videoLength

 cursor = connection.cursor()
    cursor.execute(query)
    query_result = [ dict(line) for line in [zip([ column[0] for column in cursor.description], row) for row in cursor.fetchall()] ]

 """
def get_all_project(  ):
     
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM projects ")

    #rows = cur.fetchall()

    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result





def get_all_media( _proj_id):

  
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM clips WHERE project_id = "+str(_proj_id)+"")

    #rows = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result

def get_clips( _proj_id):

  
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM clips WHERE isVideo = 1 AND project_id = "+str(_proj_id)+"")

    #rows = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result
#################################################################################

def get_snaps( _proj_id):

 
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM clips WHERE isVideo = 0 AND project_id = "+str(_proj_id)+"")

    #rows = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result   


def get_snap_byName(   _proj_id, _name):

    data_set=( _proj_id, _name)
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    
#SELECT * FROM clips WHERE isVideo = 0 AND project_id = 1 AND path = '/home/muji/Staged/utzo_deployment/static/snaps/ert3.mp4.jpg'
        cur =  conn.cursor()
        cur.execute("SELECT * FROM clips WHERE isVideo = 0 AND project_id = ? AND path = ? ",data_set)
        #cur.execute("SELECT * FROM clips WHERE isVideo = 0 AND project_id = 1 AND path = '/home/muji/Staged/utzo_deployment/static/snaps/ert3.mp4.jpg' ")

    #rows = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result

def get_video_byName(  proj_id,  _name):

    data_set=( proj_id, _name)
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    with conn:    

        cur =  conn.cursor()
        cur.execute("SELECT * FROM clips WHERE isVideo = 1 AND project_id = ? AND path = ? ",data_set)

   # rows = cur.fetchall()
    query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
    return query_result
    
    
################################################################################################





def insert_clip( _path,  proj_id,  _thmb_path , _isVideo ):
     data_set=( proj_id, _path,  datetime.datetime.now(), _thmb_path, _isVideo)
     directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
     conn = sqlite3.connect(directory+"/"+database)
     id_ret = None
     with conn:    

        cur =  conn.cursor()
        cur.execute("INSERT INTO clips(project_id, path, cliptime, thmb_path, isVideo ) values(?, ?, ?, ?, ?)",data_set)
        id_ret=cur.lastrowid
     conn.commit()
     conn.close()
    
     return id_ret


     ##################################


def get_clip_by_id(   _clip_id):

     # create a database connection
    conn = sqlite3.connect(database)

    try: 
        with conn: 
            

            cur =  conn.cursor()
            cur.execute("SELECT * FROM clips  WHERE id=?", (_clip_id,))

            #rows = cur.fetchall()
            query_result = [ dict(line) for line in [zip([ column[0] for column in cur.description], row) for row in cur.fetchall()] ]
        return query_result
    except:
        print("Database error")
        return None

def delete_media_files(_id):
   
     
    directory = os.path.dirname(os.path.abspath(__file__))
     # create a database connection
    conn = sqlite3.connect(directory+"/"+database)
    try: 
        with conn:    

            cur =  conn.cursor()
            cur.execute("SELECT id FROM clips WHERE id = ?", (_id, ))
            exist = cur.fetchone()
            if(not exist):
              return "not exist"
            ret = cur.execute("delete from  clips WHERE id =?", (_id,))
            conn.commit()
            cur.close()
        
        return "done" 
    except:
        print("Database error")
        return "fail"  
        
    finally:
    
        if conn:
            conn.close()
            print('SQLite Connection closed')       


###################################################################### local test 
# insert_project( _name, _details, _clip_path, _snap_path, _clip_length):
""" def main():
  #def insert_project( _id, _name, _details, _clip_path, _snap_path, _clip_length):
 # is_exist_job("31")
  
if __name__ == '__main__':
     main()                      """