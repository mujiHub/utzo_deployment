#!/usr/bin/env python3
import serial as sr
import time

if __name__ == '__main__':
    ser = sr.Serial('/dev/ttyUSB0', 9600, timeout=1)
    
    ser.reset_input_buffer()
    cnt=1099
    mystring="0"
    bst = mystring.encode()
    while True:
        ser.write(bst)
        line = ser.readline().decode('utf-8').rstrip()
        print(line)
        time.sleep(1)

