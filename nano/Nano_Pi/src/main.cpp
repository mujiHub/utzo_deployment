#include <Arduino.h>
#include <Adafruit_DS3502.h>
#include "DHT.h"

#define DHTPIN 10  
#define DHTTYPE DHT11   // DHT 11
DHT dht(DHTPIN, DHTTYPE);
Adafruit_DS3502 ds3502 = Adafruit_DS3502();
float R1 = 30000.0; 
float R2 = 7500.0;
int offset =20;// set the correction offset value

String nom = "Arduino";
String msg;
// Setup a oneWire instance to communicate with any OneWire device
  

// Pass oneWire reference to DallasTemperature library

//int cam_led = 9;
int rel_pi= 6; // Relay for pi
int rel_fan= 7; // Relay for fan
float temp_read = 0;
const int voltageSensor = A0;

void setup() {

    // Start up the library
  Serial.begin(115200);
  while (!Serial) { delay(1); }

  Serial.println("Adafruit DS3502 Test");

  if (!ds3502.begin()) {
    Serial.println("Couldn't find DS3502 chip");
    while (1);
  }
  Serial.println("Found DS3502 chip");



 // pinMode(cam_led, OUTPUT);
  dht.begin();
  pinMode(rel_pi, OUTPUT);
  pinMode(rel_fan, OUTPUT);
  digitalWrite(rel_pi, HIGH); // This means make PI ON
  digitalWrite(rel_fan, LOW); // This means make Fan ON
  //analogWrite(cam_led, 255);

  
}

/*
value = analogRead(analogInput);
  vout = (value * 5) / 1024.0;
  vin = vout / (R2/(R1+R2));
  vin= vin+0.93;
*/
void loop() {
  int volt = analogRead(A0);// read the input
  double  vout = (volt * 5) / 1024.0;
  double  voltage = vout / (R2/(R1+R2))+0.93;

 
 
 
   // Send the command to get temperatures

  temp_read =  dht.readTemperature();
  if(temp_read >= 35 )
   digitalWrite(rel_fan, LOW); // This means FAN is ON now 
  if(temp_read <= 30 )
   digitalWrite(rel_fan,  HIGH); // This means FAN is OFF now 
  if(temp_read >= 50 ) 
   digitalWrite(rel_pi, LOW); // This means Make PI  off NOW
  if(temp_read < 45 ) 
    digitalWrite(rel_pi,  HIGH); // This means Make PI is ON NOW 


  //make fan ON 



  //print the temperature in Celsius
  Serial.println("Temperature: "+String(temp_read)+" :Voltage: "+String(voltage));
  //Serial.println( temp_read);
   delay(1000);
  //Serial.print("Voltage: ");
  //Serial.println(voltage);//print the voltge
 
  

  if (Serial.available() > 0) {
      String data = Serial.readStringUntil('\n');
      Serial.println("Led Power sent: by WEB Portal ");
      Serial.println(data);
     // analogWrite(cam_led, data.toInt());
    //  ds3502.setWiper(data.toInt());
     
      
    }
  
  
}