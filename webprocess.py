
# Version 3.5

from http.client import OK
from app_settings import is_mounted
from models import *


######################################
import os
import asyncio
from arduino_con import *


import uvicorn as uvicorn
from fastapi import FastAPI, APIRouter, HTTPException , File, UploadFile
from fastapi.requests import Request
from fastapi.responses import HTMLResponse
from fastapi.responses import StreamingResponse, FileResponse
from fastapi.templating import Jinja2Templates
from fastapi.responses import Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from typing import Union
from fastapi.responses import HTMLResponse
from fastapi import Header
from pathlib import Path

from app_settings import *
from msg_camera import *

import os.path
from os import path
CHUNK_SIZE = 1024*1024
#####################################




app = FastAPI(
    title="Recipe API", openapi_url="/openapi.json"
)


api_router = APIRouter()



########################################################################----------------------------API router----------------###################################
 

    


######################
# mount Directory 
app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount("/images", StaticFiles(directory="images"), name="images")
app.mount("/media", StaticFiles(directory="media"), name="media")




templates = Jinja2Templates(directory="templates/")
origins = [
    "http://localhost",
    "http://localhost:5000",
    "http://localhost:5500",
    "http://127.0.0.1:5500",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)






""" 
https://github.com/harshmangalam/faststream-server
https://github.com/MattWeinberg24/pi-stream/tree/master


# Recordings
    app.mount("/recordings",
              StaticFiles(directory=settings.recording_files_dir),
              name="recordings")

     """
      
  

@app.get("/", response_class=HTMLResponse)
async def get_main(request: Request):
    return templates.TemplateResponse("index_new.html", {"request": request})

""" 
@app.get("/sensor")
async def get_index(request: Request):
    return templates.TemplateResponse("main.html", {"request": request}) """


@app.get("/top")
async def get_index(request: Request):
    return templates.TemplateResponse("header.html", {"request": request})
    
@app.get("/left")
async def get_index(request: Request):
    return templates.TemplateResponse("sidebar.html", {"request": request}) 


@app.get("/player")
async def get_index(request: Request):
    return templates.TemplateResponse("player.html", {"request": request})  

@app.get("/usb_drive_status")
async def get_drive(request: Request):
   # return is_mounted()  
    return {"status":"true", "message": is_mounted()}  





@app.get("/add_project/")
async def insert_poject(id: str, name: str ): 

    base_path= recording_files_dir.strip() 
    project_path = base_path +"/"+name.strip()
    res = insert_job( id.strip() ,name.strip() ,  project_path )
    if("success" == res):

     if(not os.path.isdir( project_path)):
        os.makedirs(project_path)
        if(not os.path.isdir(project_path+"/videos")):
         os.makedirs(project_path+"/videos")
         
        if(not os.path.isdir(project_path+"/snaps")):
         os.makedirs(project_path+"/snaps")
       
        return {"status":"true" ,"message": res}  
    else:

        return {"status":"false" ,"message": "failed"}  




  # insert_project( id.strip() ,name.strip() , details.strip(), media_path.strip() )

 

    return {"message": "Added"}  
""" 
exist = cursor.fetchone()
if exist is None:
  ... # does not exist
else:
  ... # exists
 """

@app.get("/get_projects")
async def get_pojects():
    res = get_all_project()
    if not res:
        raise HTTPException(
                {"status":"false"}
            )
    return {"status":"true" ,"message": res}  


@app.get("/update_project/")
async def insert_poject(project_id: int , name: str, details: str, clip_path: str, snap_path: str , clip_length: int): 
    res =update_project(project_id ,name, details, clip_path, snap_path, clip_length)
    if not res:
        raise HTTPException(
               {"status":"false"}
            )
    return {"status":"true" ,"message": "updated"}  

#################################################



########################################  end file to client 




@app.get("/voice_over_on/")
async def start_voice_over(details: str = "na"): 

    return {"status":"true" ,"message": {"resp" : "started"}} 

@app.get("/voice_over_off/")
async def stop_voice_over(details: str ="na"): 
    return {"status":"true" ,"message": {"resp" : "stoped"}} 
#################################################

@app.get("/get_stream/")
async def get_video(video_id: int):

    file_path = get_file_media_path(video_id)[0].strip()
    video_path = file_path
    if video_path:
        return StreamingResponse(open(video_path, 'rb'))
    else:
        return Response(status_code=404)


###############


@app.get("/file_send/")
async def get_media_file(f_id: str, range: str = Header(None) ):     
    
    file_path = get_file_media_path(f_id)[0].strip()
    fn = os.path.basename(file_path)
    response = FileResponse(file_path, media_type="application/octet-stream", filename=fn)
    return response

################################################
"""            
     resp = FileResponse(file_path, media_type="application/octet-stream", filename=fn)
 
    return {"status":"true" ,"message": {"file" : resp}}  
 """

################################################
@app.get("/record_video/")
async def record_item(proj_id: str, f_nm: str = "na"):

    ret = create_job(proj_id)
    if("success" == ret or "already exist"  == ret): 
        if(f_nm == "na"):
                f_nm =get_file_name(str(proj_id))

        file_path = get_project_media_path(proj_id) +"videos/"+f_nm+".mp4"
    
        
        msg = "Record, "+ str(proj_id) +", "+file_path+", default"
        
        msg_cam(msg)
            
        return {"status":"true" ,"message": {"resp" : "Recording Started "}}  
    return {"status":"false" ,"message": {"resp" : "Recording Not Started "}}      

################################################################
@app.get("/stop_recording/")
async def stop(proj_id: str):
        f_nm =get_file_name(str(proj_id))
        msg="Stop, it"
        
        msg_cam(msg)
        return {"status":"true" ,"message": "Recording Stoped "}  


########################################################### snap
@app.get("/snap_image/")
async def snap( proj_id: str, f_nm: str = "na"):

        
        ret = create_job(proj_id)
        if("success" == ret or "already exist"  == ret):
            if(f_nm == "na"):
                f_nm =get_file_name(str(proj_id))
            file_path = get_project_media_path(proj_id) +"snaps/"+f_nm+".jpg"
        

            msg="Snap, "+str(proj_id)+", "+file_path
            msg_cam(msg)            
            return {"status":"true" ,"message":  {"resp" : "Snap Done"}}  
            
        return {"status":"false" ,"message":  {"resp" : "Snap Error"}}  

#################################################################################check if ID exist if not make it 
#@app.get("/job_it/")
def create_job(job_id):
 
    name = job_id.strip()
    base_path= recording_files_dir.strip() 
    project_path = base_path +"/"+name.strip()
    res = insert_job( job_id.strip() ,name.strip() ,  project_path )
    if("success" == res ):

     if(not os.path.isdir( project_path)):
        os.makedirs(project_path)
        if(not os.path.isdir(project_path+"/videos")):
         os.makedirs(project_path+"/videos")
         
        if(not os.path.isdir(project_path+"/snaps")):
         os.makedirs(project_path+"/snaps")
    return res

################################################################################# Get all media of Job id


@app.get("/get_all_media/")
async def get_all_recordings(proj_id: int):
    res = get_all_media(proj_id)
    if not res:
        raise HTTPException(
               {"status":"false"}
            )

    return {"status":"true" ,"message": res}    
#######################################################################


@app.get("/delete_media/")
async def delete_media_list(clip_ids: str):

    resp_dic = {}
    id_tokens ={}
    if(',' in clip_ids):
        id_tokens = clip_ids.split(",")
    else:
        id_tokens ={clip_ids}    
    for id in id_tokens:
        resp_dic[id] = delete_media_files(id.strip())
    
    return {"status":"true" ,"message": resp_dic} 


    """   res = delete_media(clip_id)
    if not res:
        raise HTTPException(
              {"status":"false"}
            )

    return {"status":"true" ,"message": res}    """       

########################################################################
@app.get("/get_videos/")
async def get_recordings(proj_id: int):
    res = get_clips(proj_id)
    if not res:
        raise HTTPException(
               {"status":"false", }
            )

    return {"status":"true" ,"message": res}  
  

@app.get("/get_snaps/")
async def get_snaps_project(proj_id: int):
    res = get_snaps(proj_id)
    if not res:
        raise HTTPException(
               {"status":"false"}
            ) 


    return {"message": res}    




@app.get("/get_snap_byName/{proj_id}/")
async def get_item(proj_id: int, f_nm: str):
    res = get_snap_byName(proj_id,f_nm)  #get_snaps(CAM_ID)
    if not res:
        raise HTTPException(
              {"status":"false"}
            ) 
    return {"message": res}   


@app.get("/get_video_byName/{proj_id}/")
async def get_item(proj_id: int, f_nm: str):
    res = get_video_byName(proj_id,f_nm)  #get_snaps(CAM_ID)
    if not res:
        raise HTTPException(
             {"status":"false"}
            ) 
    return {"status":"true" ,"message": res}  
#####################################_______________________Routers_______________###############






#####################################____________________Hardware status Routers_______________###############



@app.get("/get_temp/")
async def read_vales():
   

   #return "'Temperature: -255.00 :Voltage: 8.86'"
   nano_val ='Temperature: 100.00 :Voltage: 9.86' #str(read_nano()) # str(read_nano()) #
   print(nano_val)
   if(nano_val is not None):
        tokens = nano_val.split(':')
        ret_val = [tokens[1], tokens[3]]
    
       # return ret_val
   return {"status":"true" ,"message": {"temprature": 35}}   


      
    

@app.get("/set_led/")    
async def send_to_nano(val: int ):
    asyncio.sleep(1)
    print("Message for NANO "+str(val))
    write_nano(val)
    return {"status":"true" ,"message": "done"}   

    
@app.get("/get_brightnes/")
async def read_led():
    return {"status":"true" ,"message": {"brightness": 60}}   

@app.get("/get_usb_list/")
async def read_usb():
   
    return {"status":"true" ,"message": {"path": recording_files_dir}}      


@app.get("/get_battery_level/")
async def read_battery():
    return {"status":"true" ,"message": 45}  

@app.get("/get_battery_charging/")
async def read_battery_status():
    return {"status":"true" ,"message": {"charging" : False}} 


#####################################_______________________Routers_______________###############



############################################################################Api runner 
def run_api():

    
    uvicorn.run(app, host="0.0.0.0", port=10000)  
   # uvicorn.run(app, host="127.0.0.1", port=5000, reload=True)
    print("Web API running")



